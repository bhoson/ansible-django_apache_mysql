#!/bin/bash
NEW_ROOT_PASS=$1
temp_pass=$(grep 'temporary password' /var/log/mysqld.log | awk '{print $11}')
mysql -p$temp_pass --connect-expired-password -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '$NEW_ROOT_PASS';"


